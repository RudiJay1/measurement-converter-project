# README #

Thank you for looking at my repository.

### What is this project? ###

This is a demonstration of my algorithmic ability. My goal was to create a program that will read in the contents of a text file as measurements and conversions, and then allow the user to enter amounts and have their measurements converted. Measurements can be entered in either order and the measurements don't need to be entered into the text file more than once to show the same conversion (e.g. miles to feet and feet to inches will still convert miles to inches).

### How do I get set up? ###

1. Clone the repo.
2. Open "Converter\Converter.sln" in the repo folder.
3. Run the project.
4. The measurement and their conversion rates are stored at "Converter\Converter\bin\Debug\convert.txt". To add or remove measurements, open this file and add them in the format "{measurement 1}, {measurement 2}, {conversion rate}" with any capitalisation or spacing.

### Who do I contact the author? ###

http://www.rudiprentice.com/contact.html