﻿using System;
using System.IO;

namespace SOFT153_Algorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            Converter converter = new Converter();

            converter.RunProgram();
        }
    }

    //A single element of a list
    //holds the pointer to the next node and the three parts of a conversion
    class Node
    {
        public string unit1;
        public string unit2;
        public double conversionFactor;

        public Node next;
    }

    //List that holds its first node, and methods to find and set a new last node
    class List
    {
        public Node firstNode;

        //traverse list until null and return the reference to the last node
        public Node GetLast()
        {
            Node lastNode = firstNode;

            while (lastNode.next != null)
            {
                lastNode = lastNode.next;
            }

            return lastNode;
        }

        //add a new node to the end of a linked list, or if there are no nodes, add it to the beginning
        public void AddLast(Node newLastNode)
        {
            if (firstNode != null)
            {
                GetLast().next = newLastNode;
            }
            else
            {
                firstNode = newLastNode;
            }
        }
    }

    class Converter
    {
        static List conversionsList = new List(); //holds the conversions from the conversion files

        public void RunProgram()
        {
            GetConversionFile();

            GetInput();
        }

        /// <summary>
        /// finds the conversion text file
        /// reads through the file one line at a time...
        /// splits the line into three parts, then adds them to array for that line
        /// if all three parts are present for that line and the third part is a double value
        /// create a new node storing the three parts and add it to the end of the conversion list
        /// </summary>
        private void GetConversionFile()
        {
            string filePath = Path.Combine(Environment.CurrentDirectory, "convert.txt"); //pathway to conversion file

            string[] splitLine = new string[3]; //array to temporarily hold two units and conversion factor on a line
            string[] delimiter = new string[] { ", ", "," }; //array used to as delimiter to split input into three parts

            using (StreamReader reader = new StreamReader(filePath))
            {
                while (!reader.EndOfStream)
                {
                    string lineFromFile = ConvertLowerCase(reader.ReadLine()); //read a line from file and convert it to lower case

                    splitLine = lineFromFile.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

                    if (splitLine.Length == 3) //check the line has all three items to read (IE not a blank line)
                    {
                        try
                        {
                            Node newItem = new Node();

                            newItem.unit1 = splitLine[0];
                            newItem.unit2 = splitLine[1];
                            newItem.conversionFactor = Convert.ToDouble(splitLine[2]);

                            conversionsList.AddLast(newItem);
                        }
                        catch
                        {
                            Console.WriteLine("ERROR reading conversion file! A conversion was not added because its third value was not in correct number format!");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Prints user instructions asking for input
        /// Continually checks for conversion input
        /// finishes if user enters negative value for input
        /// </summary>
        private void GetInput()
        {
            string[] splitInput = new string[3]; //array to hold three parts of conversion
            string[] delimiter = new string[] { ", ", "," }; //array used to as delimiter to split input into three parts

            string input;   //user's key input
            bool gettingInput = true; //whether to continue getting input from user or not

            //instructions for the user
            Console.WriteLine("\n===============================================================================" +
                    "\nPlease enter a conversion following the format of the example below:\n\n" +
                    "\t5, ounce, gram\n\n" +
                    "Where 5 ounces will be converted into grams\n" +
                    "Enter a negative conversion value to quit" +
                    "\n===============================================================================\n");

            //user input loop:
            //keep asking for input until the user enters a negative value
            //split the user's input and if it has the correct amount of values and the first one is a number, search for their input
            while (gettingInput)
            {
                Console.WriteLine("===============================================================================\n");

                input = ConvertLowerCase(Console.ReadLine()); //read user input and convert it to lower case

                splitInput = input.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

                if (splitInput.Length == 3)
                {
                    try
                    {
                        if (Convert.ToDouble(splitInput[0]) >= 0)
                        {
                            SearchList(splitInput);
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("ERROR! Invalid entry! Please make sure the first value is a number!");
                    }
                }
                else
                {
                    Console.WriteLine("ERROR! Invalid entry! Please make sure there are three values separated by a comma!");
                }
            }
        }

        /// <summary>
        /// Keeps searching until the conversion is found or definitely not there
        /// traverses the linked list searching for a matching first unit and if it's found, calls a method that checks for the second unit
        /// if the second unit is found, stop searching, otherwise, keep searching
        /// if the conversion is not found in the order inputted, swap the order around and try again
        /// if it's not found then it's not in the file
        /// if it is found call method to calculate conversion
        /// </summary>
        private void SearchList(string[] splitInput)
        {
            string unit1 = splitInput[1];
            string unit2 = splitInput[2];

            bool swappedUnits = false;
            bool searching = true;
            bool matchFound = false;

            while (searching)
            {
                Node searchNode = conversionsList.firstNode;

                while (searchNode != null)
                {
                    if (unit1 == searchNode.unit1)
                    {
                        if (CheckSecondValue(searchNode, unit2))
                        {
                            matchFound = true;
                            break;
                        }
                    }

                    searchNode = searchNode.next;
                }

                if (matchFound)
                {
                    searching = false;

                    ConvertUnits(splitInput, searchNode, swappedUnits);
                }
                else
                {
                    if (swappedUnits)
                    {
                        searching = false;

                        Console.WriteLine("ERROR: The conversion you entered is not on the conversion file!");
                    }
                    else
                    {
                        string tempUnit = unit1;
                        unit1 = unit2;
                        unit2 = tempUnit;

                        swappedUnits = true;
                    }
                }
            }
        }

        //checks whether the second string in a node is the second unit the user is searching for
        private bool CheckSecondValue(Node nodeToSearch, string searchUnit)
        {
            if (searchUnit == nodeToSearch.unit2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //converts units using ammount input by user, data on conversion factor and whether or not the two units are reversed in order
        private void ConvertUnits(string[] input, Node node, bool reverseCalc)
        {
            double answer = 0;

            if (!reverseCalc)
            {
                answer = Convert.ToDouble(input[0]) * node.conversionFactor;

                Console.WriteLine("{0} {1}s is {2} {3}s (1 {1} is {4} {3}s)\n", input[0], input[1], answer, input[2], node.conversionFactor);
            }
            else
            {
                answer = Convert.ToDouble(input[0]) / node.conversionFactor;

                Console.WriteLine("{0} {1}s is {2} {3}s ({4} {1}s is 1 {3})\n", input[0], input[1], answer, input[2], node.conversionFactor);
            }
        }

        //converts a string into lower case by splitting it up into single characters, changing ASCII values and returning the new string
        private string ConvertLowerCase(string input)
        {
            char[] characters = input.ToCharArray();

            for (int i = 0; i < characters.Length; i++)
            {
                int asciiValue = Convert.ToInt32(characters[i]);

                if (asciiValue >= 65 && asciiValue <= 90) //the ascii values for capital letters
                {
                    asciiValue += 32; //the ascii value difference between capital and lower case letters
                }

                characters[i] = Convert.ToChar(asciiValue);
            }

            string lowerCaseInput = new string(characters);

            return lowerCaseInput;
        }
    }
}
